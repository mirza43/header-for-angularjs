import template from './header.html!text';
import 'bootstrap/css/bootstrap.css!css'
import 'bootstrap'
import './header.css!';
import HeaderController from './controller'

function header() {
    return {
        scope: {
            isLoginRequired: '&'
        },
        replace: true,
        controller: HeaderController,
        controllerAs: 'controller',
        bindToController: true,
        template: template
    }
}

export default header;